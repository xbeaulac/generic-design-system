import { defineStore } from "pinia";
import { computed, ComputedRef, onUnmounted, Ref, ref, watch } from "vue";

export type textType =
  | "display"
  | "h1"
  | "h2"
  | "h3"
  | "h4"
  | "h5"
  | "h6"
  | "p1"
  | "p2"
  | "p3"
  | "caps1"
  | "caps2"
  | "caps3";

export type spacingType =
  | "s"
  | "s2"
  | "s3"
  | "s4"
  | "s5"
  | "s6"
  | "s8"
  | "s10"
  | "s12"
  | "s16"
  | "s20"
  | "s24"
  | string;

export const useUIStore = defineStore("ui", () => {
  const theme = ref(document.documentElement.attributes[1].value);
  const toggleTheme = () => {
    document.documentElement.classList.remove(theme.value);
    theme.value = theme.value === "light" ? "dark" : "light";
    document.documentElement.setAttribute("data-theme", theme.value);
    document.documentElement.classList.add(theme.value);
  };

  // type - { hash: [styleString, numUsing] }
  const allStyles: Ref<{
    [key: string]: { styleString: string; numUsing: number };
  }> = ref({});

  // getStyleTag() gets the <style> tag with the appropriate title attribute in the <head>.
  // elName - name of the element to look up in the <head> (ex. "XButton")
  const getStyleTag = () => {
    const head = document.getElementsByTagName("head")[0];
    for (const style of head.children) {
      if (style.attributes.length > 0 && style.attributes[0].value === "X") {
        return style; // If <style title="XGrid"> exists inside <head>, return it
      }
    }
    const style = document.createElement("style");
    style.title = "X";
    document.getElementsByTagName("head")[0].appendChild(style);
    return style; // If no <style title="XGrid"> exists, create one and return it
  };

  // kebabize() converts strings from camelCase to kebab-case
  // str - the input string
  const kebabize = (str: string) =>
    str.replace(
      /[A-Z]+(?![a-z])|[A-Z]/g,
      ($, ofs) => (ofs ? "-" : "") + $.toLowerCase()
    );

  // getHash() generates hexadecimal hashes based on a string
  const getHash = (str: string) => {
    let h = 9;
    for (let i = 0; i < str.length; i++) {
      h = Math.imul(h ^ str.charCodeAt(i++), 9 ** 9);
    }
    return Math.abs(h ^ (h >>> 9)).toString(16);
  };

  // based on a styles object, it makes a string of valid css
  const getStyleString = (styles: {
    [key: string]: string | ComputedRef | Ref | undefined;
  }) => {
    const styleString = Object.entries(styles)
      .filter(([, v]) => {
        if (typeof v === "string") return true;
        if (typeof v === "undefined") return false;
        return v.value !== undefined;
      })
      .map(([key, value]) => [
        kebabize(key),
        typeof value !== "string" ? value?.value : value,
      ])
      .map((item) => item.join(": "))
      .join("; ");
    return styleString ? styleString + ";" : undefined;
  };

  // setStyles() sets an object of css properties to a given selector
  // elName - name of the element to look up in the <head> (ex. "XButton")
  // styles - style object to set (a key such as backgroundColor will be converted to the background-color property)
  const setStyles = (
    el: Element,
    styles: { [key: string]: string | ComputedRef | Ref | undefined }
  ) => {
    const styleString = getStyleString(styles);
    if (styleString) {
      const hash = getHash(styleString); // generate a hash based on that string
      if (!allStyles.value[hash]) {
        allStyles.value[hash] = { styleString, numUsing: 1 };
      } else {
        allStyles.value[hash].numUsing++;
      }
      getStyleTag().innerHTML = Object.entries(allStyles.value)
        .map(
          ([hash, styleArray]) =>
            `[data-x-${hash}] { ${styleArray.styleString} }`
        )
        .join("\n\n"); // combine all strings and set that as the innerHTML of the <style> tag
      el.setAttribute(`data-x-${hash}`, ""); // add data attribute
      return hash; // returns the hash
    } else {
      return null;
    }
  };

  const updateStyles = (
    oldHash: string | null,
    el: Element,
    styles: { [key: string]: string | ComputedRef | Ref | undefined }
  ) => {
    removeStyles(oldHash, el);
    return setStyles(el, styles);
  };

  const removeStyles = (hash: string | null, el?: Element) => {
    if (hash) {
      allStyles.value[hash].numUsing--;
      el?.removeAttribute(`data-x-${hash}`);
      if (allStyles.value[hash].numUsing === 0) {
        delete allStyles.value[hash];
        getStyleTag().innerHTML = Object.entries(allStyles.value)
          .map(
            ([hash, styleArray]) =>
              `[data-x-${hash}] { ${styleArray.styleString} }`
          )
          .join("\n\n"); // combine all strings and set that as the innerHTML of the <style> tag
      }
    }
  };

  const reactiveStyles = (
    el: Element,
    styles: { [key: string]: ComputedRef | Ref | undefined }
  ) => {
    const hash: Ref<string | null> = ref(null);
    hash.value = setStyles(el, styles);

    const sources = Object.values(styles);
    watch(sources, () => {
      hash.value = updateStyles(hash.value, el, styles);
    });

    onUnmounted(() => {
      removeStyles(hash.value, el);
    });
  };

  const toCP = (prop: string | undefined) => {
    if (prop) return `var(--${prop})`;
    return undefined;
  };

  const getMargin = (props: Readonly<any>) => {
    return computed(() => {
      if (props.m) {
        return toCP(props.m);
      } else if (props.mV && props.mH) {
        return `${toCP(props.mV)} ${toCP(props.mH)}`;
      } else if (props.mV) {
        return `${toCP(props.mV)} ${toCP(props.mR) || "0"} ${toCP(props.mV)} ${
          toCP(props.mL) || "0"
        }`;
      } else if (props.mH) {
        return `${toCP(props.mT) || "0"} ${toCP(props.mH)} ${
          toCP(props.mB) || "0"
        } ${toCP(props.mH)}`;
      } else if (props.mT || props.mR || props.mB || props.mL) {
        return `${toCP(props.mT) || "0"} ${toCP(props.mR) || "0"} ${
          toCP(props.mB) || "0"
        } ${toCP(props.mL) || "0"}`;
      } else {
        return undefined;
      }
    });
  };

  const getPadding = (props: Readonly<any>) => {
    return computed(() => {
      if (props.p) {
        return toCP(props.p);
      } else if (props.pV && props.pH) {
        return `${toCP(props.pV)} ${toCP(props.pH)}`;
      } else if (props.pV) {
        return `${toCP(props.pV)} ${toCP(props.pR) || "0"} ${toCP(props.pV)} ${
          toCP(props.pL) || "0"
        }`;
      } else if (props.pH) {
        return `${toCP(props.pT) || "0"} ${toCP(props.pH)} ${
          toCP(props.pB) || "0"
        } ${toCP(props.pH)}`;
      } else if (props.pT || props.pR || props.pB || props.pL) {
        return `${toCP(props.pT) || "0"} ${toCP(props.pR) || "0"} ${
          toCP(props.pB) || "0"
        } ${toCP(props.pL) || "0"}`;
      } else {
        return undefined;
      }
    });
  };

  return {
    theme,
    toggleTheme,
    allStyles,
    getStyleTag,
    getStyleString,
    getHash,
    kebabize,
    setStyles,
    updateStyles,
    removeStyles,
    reactiveStyles,
    getMargin,
    getPadding,
  };
});
