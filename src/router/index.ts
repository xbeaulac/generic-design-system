import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import XButtonView from "../views/XButtonView.vue";
import XSelectView from "../views/XSelectView.vue";
import XToggleView from "../views/XToggleView.vue";
import XTextFieldView from "../views/XTextFieldView.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "XButton",
    component: XButtonView,
  },
  {
    path: "/select",
    name: "XSelect",
    component: XSelectView,
  },
  {
    path: "/toggle",
    name: "XToggle",
    component: XToggleView,
  },
  {
    path: "/textField",
    name: "XTextField",
    component: XTextFieldView,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
